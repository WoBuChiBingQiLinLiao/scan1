$(document).ready(function(){
  document.oncontextmenu = function(e){
      e.preventDefault();
  };
  var myChart = echarts.init(document.getElementById('main'));
  myChart.showLoading();
  window.addEventListener("resize",function(){
      myChart.resize();
  });
  myChart.on('mousedown', function (params) {
      // 控制台打印数据的名称
      console.log(params);
      if(params.event.event.button == 2){
        $('.popUp').css('height','990px')
        
        $('.popUp').show(500)

      }
  });
  $.get('../echarts/les-miserables.gexf', function (xml) {
      myChart.hideLoading();
      var graph = echarts.dataTool.gexf.parse(xml);
      var categories = [];
      for (var i = 0; i < 9; i++) {
          categories[i] = {
              name: '类目' + i
          };
      }
      graph.nodes.forEach(function (node) {
          node.itemStyle = null;
          node.value = node.symbolSize;
          node.symbolSize /= 1.5;
          node.label = {
              normal: {
                  show: node.symbolSize > 30
              }
          };
          node.category = node.attributes.modularity_class;
      });
      option = {
          title: {
              text: 'Les Miserables',
              subtext: 'Default layout',
              top: 'bottom',
              left: 'right'
          },
          tooltip: {},
          legend: [{
              // selectedMode: 'single',
              data: categories.map(function (a) {
                  return a.name;
              })
          }],
          animationDuration: 1500,
          animationEasingUpdate: 'quinticInOut',
          series : [
              {
                  name: 'Les Miserables',
                  type: 'graph',
                  layout: 'none',
                  data: graph.nodes,
                  links: graph.links,
                  categories: categories,
                  roam: true,
                  label: {
                      normal: {
                          position: 'right',
                          formatter: '{b}'
                      }
                  },
                  lineStyle: {
                      normal: {
                          color: 'source',
                          curveness: 0.3
                      }
                  }
              }
          ]
      };

      myChart.setOption(option);
  }, 'xml');
})

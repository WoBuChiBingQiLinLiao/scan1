$(document).ready(function(){

      var encodeStr = window.btoa('Test_1:zxcdsa')
      var color = '123456789abcdef'
      var settings = {
      async: true,
      type: "POST",
      url: "https://api.wallsdoma.com/users/authentication",
      error: window.errorFun,
      success: function (data) {
          if (data.code === 0) {
              var stateData = data
                  $.ajax({
                      type: "GET",
                      url: "https://api.wallsdoma.com/tasks",
                      data:'order=id&&desc=sort&&page=1&&step=50',
                      success: function(result) {
                          if(result.code == 0){
                            console.log(result);
                            for(let i = 0; i < result.data.length; i++){
                              var divTag = $('<div></div>')
                              divTag.attr('class','result_detail')
                              var span = $('<span></span>')
                              span.attr('class','icon-cross')
                              span.css({
                                display:'inline-block',
                                margintop:'10px',
                                color:'gray'
                              })
                              var p = $('<p></p>')
                              p.append(span)
                              divTag.append(p)
                              var divUser = $('<div></div>')
                              var user = result.data[i].user
                              divUser.text(user[0])
                              divUser.attr('class','divUser')
                              divTag.append(divUser)
                              var labelUser = $('<label></label>')
                              labelUser.text(result.data[i].user)
                              labelUser.attr('class','userLabel')
                              divTag.append(labelUser)
                              var labelTime = $('<label></label>')
                              labelTime.text(result.data[i].create_time)
                              labelTime.attr('class','labelTime')
                              divTag.append(labelTime)
                              var pPamars = $('<p></p>')
                              var labelPamars = $('<label></label>')
                              labelPamars.text('任务参数:')
                              pPamars.append(labelPamars)
                              var labelTitle = $('<label></label>')
                              labelTitle.text(result.data[i].parameter)
                              // pPamars.append(labelTitle)
                              pPamars.attr('class','pPamars')
                              divTag.append(pPamars)

                              // 菜单
                              var pTab = `
                              <div class="tabs">

                                <p class="p"><span class="icon-cross" title="删除"></span></p>
                                <p class="p"><span class="icon-plus" title="添加"></span></p>
                                <p class="p"><a class='a' href='../../1.html' title='查看'><span class="icon-frustrated"></span></a></p>
                              <span class="icon-circle-left"></span>

                              </div>`

                              divTag.append(pTab)
                              // 动画效果
                              // $('.tabs>span').mouseover(function(){
                              //   console.log('111');
                              //     $('.p1').show(20)
                              //     $('.p2').show(50)
                              //     $('.p2').show(100)
                              //
                              // })
                              $('.container_result').append(divTag)

                            }
                          }
                      },
                      beforeSend: function(xhr) {
                          xhr.setRequestHeader("Token",data.token );
                          xhr.setRequestHeader("Hmac",data.hmac );
                      }
                  })

            }
      },
      beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Basi ${encodeStr}`);
      }
  };
      $.ajax(settings);
})
